set objectDetection_replace_folder=entry\build\default\outputs\default\entry-default-signed.hap

cd ..

set hdc=hdc.exe

%hdc% shell "mount -o rw,remount /"
%hdc% shell "rm -rf /system/app/com.ohos.mindspore.ai/ObjectDetection.hap"

%hdc% file send %objectDetection_replace_folder% /system/app/com.ohos.mindspore.ai/ObjectDetection.hap

%hdc% shell "rm -rf /data/*"
%hdc% shell "reboot"