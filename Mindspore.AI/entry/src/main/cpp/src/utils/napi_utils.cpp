/*
 * Copyright 2023 Unionman Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "napi_utils.h"
#include "napi_error.h"
#include "mslite_errors.h"
#include "mslite_log.h"

namespace OHOS {
namespace MSAI {
namespace {
constexpr int SIZE = 1048576;
}

static int memcpy_s(void *dest, size_t destSize, const void *src, size_t count) {
    if (count == 0) {
        return 0;
    }

    if (dest == nullptr || src == nullptr) {
        return -1;
    }

    if (destSize < count) {
        return -1;
    }

    char *dst = (char *)dest;
    const char *srcPtr = (const char *)src;

    while (count-- > 0) {
        *dst++ = *srcPtr++;
    }

    return 0;
}

bool GetNapiInt32(const napi_env &env, const int32_t value, napi_value &result)
{
    napi_status ret = napi_create_int32(env, value, &result);
    if (ret != napi_ok)
    {
        LOGE("GetNapiInt32 failed");
        return false;
    }
    return true;
}

bool GetPropertyString(const napi_env &env, const napi_value &value, const std::string &type, std::string &result)
{
    napi_value item = nullptr;
    bool exist = false;
    char buffer[SIZE];
    size_t length = 0;

    napi_status status = napi_has_named_property(env, value, type.c_str(), &exist);
    if (status != napi_ok || !exist) {
        LOGW("can not find %{public}s", type.c_str());
        return false;
    }

    if (napi_get_named_property(env, value, type.c_str(), &item) != napi_ok) {
        LOGW("fail to get property: %{public}s", type.c_str());
        return false;
    }

    if (napi_get_value_string_utf8(env, item, buffer, SIZE, &length) != napi_ok) {
        LOGW("fail to get property value: %{public}s", type.c_str());
        return false;
    }
    result = std::string(buffer);
    return true;
}

bool GetPropertyInt32(const napi_env &env, const napi_value &value, const std::string &type, int32_t &result)
{
  napi_value item = nullptr;
  bool exist = false;
  napi_status status = napi_has_named_property(env, value, type.c_str(), &exist);

  if (status != napi_ok || !exist) {
    LOGW("can not find  %{public}s ", type.c_str());
    return false;
  }

  if (napi_get_named_property(env, value, type.c_str(), &item) != napi_ok) {
    LOGW("fail to get property: %{public}s ", type.c_str());
    return false;
  }

  if (napi_get_value_int32(env, item, &result) != napi_ok) {
    LOGW("fail to get property value: %{public}s ", type.c_str());
    return false;
  }
  return true;
}

void SetValueUtf8String(const napi_env &env, const std::string &fieldStr, const std::string &str,
                        napi_value &result)
{
    napi_value value = nullptr;
    napi_create_string_utf8(env, str.c_str(), NAPI_AUTO_LENGTH, &value);
    napi_set_named_property(env, result, fieldStr.c_str(), value);
}

void Uint8ArrayToJsValue(const napi_env &env, std::vector<uint8_t> &uint8Buffer, size_t bufferSize, napi_value &result)
{
    uint8_t *nativeArraybuffer = nullptr;
    napi_create_arraybuffer(env, bufferSize, reinterpret_cast<void **>(&nativeArraybuffer), &result);

    int32_t ret = memcpy_s(nativeArraybuffer, bufferSize, uint8Buffer.data(), bufferSize);
    if (ret != 0)
    {
        LOGE("memcpy_s failed\n");
        return;
    }
}

} // namespace MSAI
} // namespace OHOS