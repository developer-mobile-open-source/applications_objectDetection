/*
 * Copyright 2023 Unionman Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MS_AI_DATA_TYPE
#define MS_AI_DATA_TYPE

#include "stdio.h"
#include <string.h>

const int OBJ_NUMB_MAX_SIZE = 100;

struct LetterBox
{
    int inWidth = 0;
    int inHeight = 0;
    int targetWidth = 0;
    int targetHeight = 0;
    int channel = 3;

    float imgWHratio = 1.0;
    int targetWHratio = 1;
    float resizeScaleW = 0.0;
    float resizeScaleH = 0.0;
    int resizeWidth = 0;
    int resizeHeight = 0;
    int hPadTop = 0;
    int hPadBottom = 0;
    int wPadLeft = 0;
    int wPadRight = 0;

    bool reverseAvailable = false;
};

struct PicDesc {
    size_t width = 0;
    size_t height = 0;
    size_t dataSize = 0;
};

struct ObjectDesc {
    float left = 0;
    float top = 0;
    float right = 0;
    float bottom = 0;
    float prop = 0.0;
    std::string name = "";   
};

struct InferResult {
    ObjectDesc objects[OBJ_NUMB_MAX_SIZE] = {};
    int count = -1;
};

#endif  // MS_AI_DATA_TYPE