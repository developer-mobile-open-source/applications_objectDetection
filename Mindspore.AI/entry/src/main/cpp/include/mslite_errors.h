/*
 * Copyright 2023 Unionman Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MS_AI_ERRORS_H
#define MS_AI_ERRORS_H

#include <cstdint>
#include "mslite_log.h"

enum ErrorCode {
    // General error code
    RETCODE_SUCCESS = 0,
    RETCODE_FAILURE = -1,
    // Common error code(1001~1099)
    RETCODE_OUT_OF_MEMORY = 1001,
    RETCODE_NULL_PARAM = 1002,
    RETCODE_INVALID_PARAM = 1003,

    // Proxy error code(1100~1199)
    RETCODE_PROXY_SEND_REQUEST_FAILED = 1100,
    RETCODE_GET_SA_MANAGER_FAILED = 1101,
    RETCODE_INIT_CLIENT_FAILED = 1102,

    // Mindspore error code(1200~1299)
    RETCODE_MINDSPORE_CREATE_CONTEXT_FAILED = 1201,
    RETCODE_MINDSPORE_CREATE_CPU_DEVICE_INFO_FAILED = 1202,
    RETCODE_MINDSPORE_CREATE_NNRT_DEVICE_INFO_FAILED = 1203,
    RETCODE_MINDSPORE_CREATE_MODEL_FAILED = 1204,
    RETCODE_MINDSPORE_BUILD_MODEL_FAILED = 1205,
    RETCODE_MINDSPORE_MODEL_NUM_OUT_OF_RANGE = 1206,
    RETCODE_MINDSPORE_MODEL_GET_INPUT_FAILED = 1207,
    RETCODE_MINDSPORE_TENSOR_GET_MUTABLE_DATA_FAILED = 1208,
    RETCODE_MINDSPORE_MODEL_PREDICT_FAILED = 1209,

    // opencv error code(1300~1399)
    RETCODE_OPENCV_IMDECODE_FAILED = 1301,

    // js error code(1400~1499)
    RETCODE_JS_PARSE_INPUT_FAILED = 1401,
    RETCODE_JS_NOT_EXISTED_PARAM = 1402,
    RETCODE_JS_INVALID_OPERATION = 1403,
};

#define CHKPTR(cond, r)                                                                     \
    do                                                                                      \
    {                                                                                       \
        if ((cond) == nullptr)                                                              \
        {                                                                                   \
            LOGE("CHKPTR(%{public}s) is null, return value is %{public}d", #cond, r); \
            return r;                                                                       \
        }                                                                                   \
    } while (0)

#define CHKVAL(cond, r)                                                                 \
do                                                                                      \
{                                                                                       \
    if ((cond) == 0)                                                                    \
    {                                                                                   \
        LOGE("CHKVAL(%{public}s) is null, return value is %{public}d", #cond, r); \
        return r;                                                                       \
    }                                                                                   \
} while (0)

#define CHKID(cond, r)                                                                     \
do                                                                                         \
{                                                                                          \
    if ((cond) < 0)                                                                        \
    {                                                                                      \
        LOGE("CHKVAL(%{public}s) is invalid, return value is %{public}d", #cond, r); \
        return r;                                                                          \
    }                                                                                      \
} while (0)

#define CHKCF(cond, errDesc)                    \
    do {                                        \
        if (!(cond)) {                          \
            LOGE("%{public}s", #errDesc); \
            return false;                       \
        }                                       \
} while (0)
#endif // MS_AI_ERRORS_H