/*
 * Copyright 2023 Unionman Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MS_AI_NAPI_ERROR_H
#define MS_AI_NAPI_ERROR_H

#include <map>

#include "napi/native_api.h"

namespace OHOS {
namespace MSAI {
napi_value CreateBusinessError(const napi_env &env, const int32_t errCode, const std::string &errMessage);

void ThrowErr(const napi_env &env, const int32_t errCode, const std::string &printMsg);

std::optional<std::string> GetNapiError(int32_t errorCode);

} // namespace MSAI
} // namespace OHOS
#endif // MS_AI_NAPI_ERROR_H