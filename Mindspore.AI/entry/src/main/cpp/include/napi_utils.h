/*
 * Copyright 2023 Unionman Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MS_AI_NAPI_UTILS_H
#define MS_AI_NAPI_UTILS_H

#include <cstring>
#include <iostream>
#include <map>
#include <optional>

#include "napi/native_api.h"
#include "mslite_errors.h"
#include "mslite_data_type.h"

namespace OHOS {
namespace MSAI {

struct PicDescNapi {
    int width = 0;
    int height = 0;
    int dataSize = 0;
};

struct BoxRectNapi
{
    int left = 0;
    int right = 0;
    int top = 0;
    int bottom = 0;
};

struct ObjectDescNapi {
    BoxRectNapi box = {};
    float prop = 0.0;
    int class_index = 0;   
};

struct InferResultNapi {
    int index = 0;
};

enum AsyncErrorCode
{
    FAIL = -1,
    UNKNOW = 0,
    SUCCESS = 1
};

struct MsAIAsyncContext {
    napi_async_work asyncWork;
    napi_deferred deferred = nullptr;
    napi_ref callbackRef = nullptr;

    AsyncErrorCode status = UNKNOW;
    
    /* input data */
    std::vector<unsigned char> data;
    int32_t modelId = -1;
    int32_t picNo = 0;    
    PicDesc picDesc {};
    
    /* output data */
    InferResult inferResult{};
};

bool GetNapiInt32(const napi_env &env, const int32_t value, napi_value &result);
bool GetPropertyString(const napi_env &env, const napi_value &value, const std::string &type, std::string &result);
bool GetPropertyInt32(const napi_env &env, const napi_value &value, const std::string &type, int32_t &result);
void Uint8ArrayToJsValue(const napi_env &env, const uint8_t *srcData, size_t bufferSize, napi_value &result);
void SetValueUtf8String(const napi_env &env, const std::string &fieldStr, const std::string &str, napi_value &result);

} // namespace MSAI
} // namespace OHOS
#endif // MS_AI_NAPI_UTILS_H